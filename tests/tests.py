
import pytest
from httpx import AsyncClient
from app.server.routes.vacancy import router
from app.server.app import app




@pytest.mark.asyncio
async def test_root():
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Welcome to this fantastic app!"}\


# @pytest.mark.asyncio
# async def test_router():
#     async with AsyncClient(app=router, base_url="http://test") as ac:
#         response = await ac.get("/?title=java&pub_date=2019-04")
#     assert response.status_code == 200
#     assert response.json() == {"message": "Welcome to this fantastic app!"}
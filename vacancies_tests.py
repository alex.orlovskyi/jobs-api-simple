import tests_algorithms as tests_alg
import pytest
from httpx import AsyncClient
from app.server.routes.vacancy import router


@pytest.mark.asyncio
async def test_locations():
    async with AsyncClient(app=router, base_url="http://test") as ac:
        response = await ac.get("/")
    assert response.status_code == 200
#     assert tests_alg.check_locations(result, locations)
#
#
# @pytest.mark.asyncio
# def test_categories():
#     assert tests_alg.check_categories(result, categories)
#
#
# @pytest.mark.asyncio
# def test_fields():
#     assert tests_alg.check_fields(result)
#
#
# @pytest.mark.asyncio
# def test_json():
#     assert tests_alg.check_json_format(result)

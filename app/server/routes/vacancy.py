from fastapi import APIRouter, Body, HTTPException, Request
from fastapi.templating import Jinja2Templates
from fastapi.encoders import jsonable_encoder
from typing import Optional

from app.server.db import (
    retrieve_vacancies_by_filters,
)
from app.server.models.vacancy import (
    ErrorResponseModel,
    ResponseModel,
    VacancySchema,
)

templates = Jinja2Templates(directory="templates")
router = APIRouter()


@router.get("", response_description="Vacancy data retrieved")
async def search_vacancy_by_filters(request: Request, category: Optional[str] = None, location: Optional[str] = None,
                                    dou_id: Optional[str] = None, pub_date: Optional[str] = None,
                                    title: Optional[str] = None):
    vacancies = await retrieve_vacancies_by_filters(category, location, dou_id, pub_date, title)
    if vacancies:
        return templates.TemplateResponse('vacancies.html', {'request': request, 'vacancies': vacancies})
        # return ResponseModel(result, "Vacancy data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "Vacancy doesn't exist.")

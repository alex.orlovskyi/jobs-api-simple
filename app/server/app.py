from fastapi import FastAPI
from app.server.routes.vacancy import router as VacancyRouter

app = FastAPI()
app.include_router(VacancyRouter, tags=['vacancy'], prefix='/vacancy')


@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to this fantastic app!"}
